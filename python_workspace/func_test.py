# 함수 정의
# def 함수명(argument_lies):
# 구현
# return return_data

# def print_n_times( value, n):
#     for i in range(n):
#         print(value)

# print_n_times(00,3)
# print_n_times(111,3)

# # 연습
# def print_n_times(n, *values):
#     for i in range(n):
#         print(values)

# # 함수사용
# print_n_times(3,'가변','매개변수')

# def print_n_time(value, n=2):
#     for i in range(n):
#         print(value)

# print_n_time("얀녕하세요")

# def print_n_times(*values, n=2):
#     # n번 반복합니다.
#     for i in range(n):
#         # values는 list 처럼 활용
#         for value in values:
#             print(value)
#         print()

# print_n_times("안녕하세요", "즐거운", "파이썬 프로그래밍", 2)

def print_n_times(*values, n=2): # 가변 매개변수: *변수명 # 기본값: 변수명=값
    for i in range(n):
        print(values, end='\t')
    print()

# 기본값에 명시된 변수명 = 사용자정의값
print_n_times(3, n=3)
print_n_times(3, "가변")
print_n_times(3, "가변", "매개변수")

def retun_test():
    print("A위치 입니다.")
    return
#    print("B위치 입니다.")

retun_test()

# return - 함수 종료
# return data - 호출한 쪽에 데이터 전달
# return data, data - tuple 하나의 객체로 전달

def check_tuple(data1, data2):
    return(data1,data2)

a = [2,3,4,4]
b = [3,3,4,2]

print(check_tuple(a,b))