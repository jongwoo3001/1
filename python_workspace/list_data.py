array_list=[20, 12, 30, 0.3, 3.2, True, False, False, 20, "simsimhe"]
print(array_list)
print(type(array_list))
array_list[-1][0]

# 리스트명.append(요소)
# 리스트명.insert(위치, 요소)
# 리스트명.extend() 원래 리스트 뒤에 새로운 리스트의 요소 모두 추가
array_list.extend([2,3,4])
print(array_list)

# 리스트에 요소 제거하기
# del 키워드 혹은 pop() 함수

del array_list[-1]
print(array_list)

array_list.pop(-1)
print(array_list)

# 특정 값으로 제거하기: remove()

# 리스트.remove(값)

array = [273, 32, 103, 7, 52]
# 리스트에 반복문을 적용합니다.
for i in array:
    # 출력합니다.
    print(i, end = '\t')

# 지수 성장 모형ㅇㄹ 사용해 세균 집락 성장을 계산
time = 0
population = 1000
growth_rate = 0.21

while population < 2000:
    population = population + growth_rate * population
    print(round(population))
    time = time + 1

print("it took {0} minutes for the bacteria to double".format(time))
print("the final population was {0} bacteria".format(round(population)))

# 할당문으로 제어값 설정
time, population, growth_rate = 0, 1000, 0.21

#  원래 수의 정확한 두배 일때 까지 멈추지 않는다.