# # import datetime

# # now = datetime.datetime.now()
# # month = now.month
# # print(month)

# # month01 = int(input("월 입력: "))
# # cond = [1,3,5,7,8,10,12]

# # # 전체 월 출력
# # if month01 in cond:
# #     print("이번 달은 {}월로 31일까지 있습니다.".format(month01))

# # elif month01 == 2:
# #     print("이번 달은 {}월로 28일까지 있습니다.".format(month01))

# # else:
# #     print("이번 달은 {}월로 30일까지 있습니다.".format(month01))

# # # 이번 달 출력
# # if (month == 2):
# #     print("이번 달은 {}월로 28일까지 있습니다.".format(month))

# # elif ((month % 2 == 1) & (month < 10)) or ((month == 10)|(month == 12)):
# #     print("이번 달은 {}월로 31일까지 있습니다.".format(month))

# # else:
# #     print("이번 달은 {}월로 30일까지 있습니다.".format(month))


# # 사용자 입력에 따른 반복
# # text = ''
# # while text != 'quit':
# #     text = input("화학식을 입력하세요 (종료하려면 'quit'): ")
# #     if text == 'quit':
# #         print('프로그램을 종료합니다.')
# #     elif text == 'H20':
# #         print('물')
# #     elif text == 'NH3':
# #         print('암모니아')
# #     elif text == 'CH4':
# #         print('메탄')
# #     else:
# #         print('알 수 없는 화합물')

# while True:
#     text = input("화학식을 입력하세요 (종료하려면 'quit'): ")
#     if text == 'quit':
#         print("프로그램을 종료합니다.")
#         break
#     elif text == 'H20':
#         print('물')
#     elif text == 'NH3':
#         print('암모니아')
#     elif text == 'CH4':
#         print('메탄')
#     else:
#         print("알 수 없는 화합물")
    

# 메뉴를 선택하세요
# 1번을 누르면  AI 수강생 등록
# 2번을 누르면  AI 수강생 목록
# INPUT으로 들어온 데이터가 1번이면 NAME, AGE, MAJOR를 DICT로 작성해서 LIST로 저장
# ai_list=[] name, age, major
# 입력받은 데이터를 ai_list로 저장
# if munu == 1 name, age, major 입력 받아서 dict로 생성
# 이 dict를 ai_list에 append해서 추가할 것
# 2번이라면 저장된 목록만큼 뿌려주면 됨

ai_list=[]

while True:

    menu = input("메뉴를 눌러주세요: ")
    if menu == '등록':
        print("Name, Age, Major를 입력하시오: ")
        name = input("Name: ")
        age = int(input("Age: "))
        major = input("Major: ")
        info = {'Name':name,'Age':age,'Major':major}

        ai_list.append(info)

    elif menu == '목록':
        print("AI 수강생 정보 출력")
        i = 0
        for num in ai_list:
            i += 1
            print(i, num)

    elif menu == '종료':
        break

    elif menu == '수정':
        value = input("수정할 사람 번호: ")
        num = ai_list[int(value) - 1]
        column = input("수정할 항목")
        re = input("수정할 내용")
        num[column] = re

    elif menu == "삭제":
        value = input("삭제할 사람 번호: ")
        ai_list.pop(int(value) - 1)
    else:
        print("재입력")



# 선생님 코드

ai_list = []

while True:
    print("============AI 수강생 관리============")
    print("1. 수강생 등록")
    print("2. 수강생 목록")
    print("3. 수강생 수정")
    print("4. 수강생 삭제")
    print("0. 종료")
    menu = input