str = ' 문자열 함수 알아보기 Hello Python '
print("lower: ",str.lower())
print("upper: ",str.upper())
print("strip: ",str.strip())

print("strip: ",str.rstrip()) # 오른쪽 문자 공백 제거
print("strip: ",str.lstrip()) # 왼쪽 문자 공백 제거


split_data="10, 20, 30, 40, 50, 60, 70, 80"
print(split_data.split(", "))