# # ais_1 = {'name':'김주희','age':24,'major':'전자공학'}
# # ais_2 = {'name':'최재호','age':30,'major':'컴공'}
# # ais_3 = {'name':'이준용','age':27,'major':'전기전자'}

# # ais = [ais_1, ais_2, ais_3]
# # ais_3['major'] = '음악'
# # print(ais)

# # print('ais 신입사원 정보')

# # for ai in ais:
# #     print('이름은 {0}, 나이는 {1}, 전공은 {2} 이다.'.format(ai['name'], ai['age'], ai['major']))

# # # # del 요소 제거

# # # del ais_1['age']
# # # del ais_2['age']
# # # del ais_3['age']

# # print('age요소 제거 후 ais 신입사원 정보')

# # for ai in ais:
# #     # print('이름은 {0}, 나이는 {1}, 전공은 {2} 이다.'.format(ai['name'], ai['age'], ai['major']))
# #     print('이름은 {0}, 전공은 {1} 이다.'.format(ai['name'], ai['major']))

# # print('ais 정보')
# # for key in ais_1:
# #     print('key: {0}, value:{1}'.format(key, ais_1[key]))


# # # # range
# # # array = [273, 32, 103, 57, 52]

# # # for i in range(len(array)):
# # #     print("{}번째 반복: {}".format(i, array[i]))


# # # for i in range(len(ais)):
# # #     print("{}번째: {}".format(i, ais[i]))

# # # for i in reversed(range(len(array))):
# # #     print("{}번째 반복: {}".format(i, array[i]))

# # # while: 초기값 선언 while(조거식): 명령문 초기값 증감
# # index = 0
# # while index < len(ais):
# #     print("{}번째 반복: {}".format(index, ais[index]))
# #     index += 1

# # index = len(ais) - 1
# # while index < 0:
# #     print("{}번째 반복: {}".format(index, ais[index]))
# #     index -= 1

# # for number in range(1,11):
# #     if number % 2 == 0:
# #         print("{}는 짝수입니다.".format(number))
# #     elif number % 2 == 1:
# #         print("{}는 홀수입니다.".format(number))

# # for i in range(1,11):
# #     if i % 2 != 0:
# #         continue
# #     print("{}는 짝수입니다.".format(i))


# # 구구단 출력
# # for i in range(1,10):
# #     for j in range(2,10):
# #         k = i*j
# #         print("{}*{}={}".format(j,i,k), end='\t')
# #     print()

# # for i in range(1,10):
# #     for j in range(2,10):
# #         print("{}*{}={}".format(j,i,i*j), end='\t')
# #     print()

# example_list = ['요소a','요소b','요소c']

# print("# 단순출력")
# print(example_list)
# print()

# # enumerate 적용
# print("# enumerate() 함수 적용 출력")
# print(enumerate(example_list))

# # list 함수로 강제 변환해 출력
# print("# list 함수로 강제 변환 출력")
# print(list(enumerate(example_list)))

# for i, value in enumerate(example_list):
#     print("{}번째: {}".format(i,value))

ais_1 = {'name':'김주희','age':24,'major':'전자공학'}
ais_2 = {'name':'최재호','age':30,'major':'컴공'}
ais_3 = {'name':'이준용','age':27,'major':'전기전자'}

ais = [ais_1, ais_2, ais_3]

for i, value in enumerate(ais):
    print("{}번째: {}".format(i,value))

# index = len(ais) - 1

# # while 조건에 도달할 때까지 순회
# while index >= 0:
#     print("{}번째 반복: {}".format(index, ais[index]))
#     index -= 1

# print("ais_1 dictionary 정보 출력: items() 사용") # 튜플 형태로 데이터가 들어감
# print(ais_1.items())

# # 반복을 사용하여 리스트 재조합하는 경우
# array = []

# # 반복문을 적용합니다.
# for i in range(0, 20, 1):
#     array.append(i * i)
# print(array)

# array = [i * i for i in range(0,20,2)] #최종결과를 앞에 작성함
# print(array)

# array = ['사과','자두','초콜릿','바나나','체리']
# output = [fruit for fruit in array if fruit != '초콜릿']

# print(output)

# # 리스트 이름 = [표현식 for 반복자 in 반복할 수 있는 것  if 조건문]