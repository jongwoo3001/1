# hello = input("인사말 입력: ")
# print(type(hello), hello)

#num = input("add number: ")
#print(int(num)+100)

num1 = int(input("첫번째 데이터 입력하세요: "))
num2 = int(input("두번째 데이터 입력하세요: "))

print("입력하신 데이터 {0}+{1} = {2} 입니다.".format(num1,num2,num1+num2))
print("입력하신 데이터 {0}-{1} = {2} 입니다.".format(num1,num2,num1-num2))
print("입력하신 데이터 {0}*{1} = {2} 입니다.".format(num1,num2,num1*num2))
print("입력하신 데이터 {0}/{1} = {2} 입니다.".format(num1,num2,num1/num2))
print("입력하신 데이터 {0}%{1} = {2} 입니다.".format(num1,num2,num1%num2))
print("입력하신 데이터 {0}//{1} = {2} 입니다.".format(num1,num2,num1//num2))